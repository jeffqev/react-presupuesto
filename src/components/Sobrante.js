import React, {Fragment} from 'react';
import {revisarPresupuesto} from '../helpers'
import PropTypes from 'prop-types'

const Sobrante = ({presupuesto, sobrante}) => {
    return ( 
        <Fragment>
            <div className="alert alert-primary">
                Presupuesto: {presupuesto}
            </div>

            <div className={ revisarPresupuesto(presupuesto,sobrante) }>
                Sobrante =  {sobrante}
            </div>
        </Fragment>
     );
}

Sobrante.propTypes = {
    presupuesto: PropTypes.number.isRequired,
    sobrante: PropTypes.number.isRequired
}
 
export default Sobrante;