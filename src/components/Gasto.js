import React from 'react'
import PropTypes from 'prop-types'

const Gasto = ({gasto}) => {
    return ( 
        <li className="gasto">
            <p>
                {gasto.gasto} <span className="gasto">$ {gasto.cantidadGasto}</span>
            </p>
        </li>
     );
}
 
Gasto.propTypes = {
    gasto: PropTypes.object.isRequired,
}

export default Gasto;