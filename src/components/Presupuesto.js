import React,{Fragment, useState} from 'react';
import Errores from './Errores';
import PropTypes from 'prop-types';

const Presupuesto = ({setPresupuesto, setSobrante, setMostarpresupuestoinicial}) => {

    //Errores
    const [cantidad, setCantidad] = useState(0)
    const [errores, setErrores] = useState(false)

    const addPresupuesto = e => {
        setCantidad(parseInt(e.target.value));
        
    }

    const Formulario = e =>{
        e.preventDefault();

        //Validar formulario
        if (cantidad < 1  || isNaN(cantidad) ) {
            setErrores(true);
            return; 

        }else{
            setErrores(false)
            setPresupuesto(cantidad);
            setSobrante(cantidad);
            setMostarpresupuestoinicial(false)
        }
    }
    return ( 
        <Fragment>
            <h2>Ingresar Presupuesto</h2>
            {errores? <Errores mensaje = {"presupuesto incorrecto"}/> : '' }
            <form
                onSubmit={Formulario}
            >
                <input 
                type="number"
                className="u-full-width"
                placeholder="Presupuesto"
                onChange= {addPresupuesto}
                />

                <input 
                type="submit"
                className="button-primary u-full-width"
                value="Definir"
                />
            </form>
        </Fragment>
     );
}
 
Presupuesto.propTypes = {
    setPresupuesto: PropTypes.func.isRequired,
    setSobrante: PropTypes.func.isRequired,
    setMostarpresupuestoinicial: PropTypes.func.isRequired

}
export default Presupuesto;