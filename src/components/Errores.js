import React from 'react';
import PropTypes from 'prop-types';

const Errores = ({mensaje}) => {
    return ( 
        <p className="alert alert-danger error">{mensaje}</p>
     );
}
 
Errores.prototype= {
    mensaje: PropTypes.string.isRequired        
}
export default Errores;