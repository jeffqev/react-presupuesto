import React,{useState} from 'react';
import Errores from './Errores';
import Shortid from 'shortid';
import PropTypes from 'prop-types';

const Formulario = ({setGasto,setHacergasto}) => {

    const [gasto, setgasto] = useState('');
    const [cantidadGasto, setCantidadGasto] = useState(0);
    const [error, setError] = useState(false)

    const addGasto = e=>{
        e.preventDefault();

        //VALIDAR
        if (cantidadGasto < 1 || isNaN(cantidadGasto) || gasto.trim() === '') {
            setError(true);
            return

        }else{
            setError(false);
        }
        //CONSTRUIR GASTO
        const objGasto = {
            gasto,
            cantidadGasto,
            id: Shortid.generate()
        }
        // SETSOBRANTE EN PADRE
        setGasto(objGasto);
        setHacergasto(true);

        // RESETEAR EL FORM
        setgasto('');
        setCantidadGasto(0);
    }

    return (  
        <form 
            onSubmit={addGasto}
        >
            <h2>Agregar gastos</h2>
            {error  ? <Errores mensaje =" Gasto incorrecto" /> : ''}
            <div className="campo">
                <label htmlFor="">Gasto</label>
                <input 
                    type="text" 
                    className="u-full-width" 
                    placeholder="Ej: transporte"
                    value={gasto}
                    onChange={e=>setgasto(e.target.value)}
                    />

            </div>

            <div className="campo">
                <label htmlFor="">Cantidad</label>
                <input 
                type="number" 
                className="u-full-width" 
                placeholder="Ej: 300"
                value={cantidadGasto}
                onChange={e=> isNaN(e.target.value) ? setCantidadGasto(e.target.value) : setCantidadGasto(parseInt(e.target.value,10)) }
                />

            </div>

            <input type="submit" className="button-primary u-full-width" value="Agregar gasto"/>


        </form>
    );
}
 
Formulario.propTypes = {
    setGasto: PropTypes.func.isRequired,
    setHacergasto: PropTypes.func.isRequired

}
export default Formulario;
