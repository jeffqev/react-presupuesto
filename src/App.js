import React, {useState, useEffect} from 'react';
import Presupuesto from './components/Presupuesto';
import Formulario from './components/Formulario'
import Lista from './components/Lista'
import Sobrante from './components/Sobrante'
import Errores from './components/Errores'

function App() {

  const [presupuesto, setPresupuesto] = useState(0);
  const [sobrante, setSobrante] = useState(0);
  const [gastos, setGastos] = useState([])
  const [mostarpresupuestoinicial, setMostarpresupuestoinicial] = useState(true);
  const [gasto, setGasto] = useState({})
  const [hacergasto, setHacergasto] = useState(false)
  const [errornegativo, setErrornegativo] = useState(false);

  useEffect(() => {

    if (hacergasto) {
      const presobrante = sobrante - gasto.cantidadGasto;


      if (presobrante >= 0) {

        setGastos([
          ...gastos,
          gasto
        ]);

        setSobrante(presobrante);
        setHacergasto(false)
        setErrornegativo(false)
        
      }else{
        setErrornegativo(true)
        setHacergasto(false)
        return

      }
      

    }
    
  }, [gasto,hacergasto,sobrante,gastos])

  // nuevo gasto 
  

  return (
    <div className="container">
      <header>
        <h1>Gastos semanales</h1>
        
        <div className="contenido-principal contenido">
          {mostarpresupuestoinicial === true ? 
          (
            <Presupuesto
            setPresupuesto = {setPresupuesto}
            setSobrante = {setSobrante}
            setMostarpresupuestoinicial = {setMostarpresupuestoinicial}
            />
          )
          : 
          (
            <div className="row">
              <div className="one-half column"> 
                <Formulario
                setGasto = {setGasto}
                setHacergasto = {setHacergasto}
                />
              </div>
              <div className="one-half column"> 
              { errornegativo ? <Errores mensaje ="No hay presupuesto"/> : ''}
                <Lista
                  gastos = {gastos}
                />

                <Sobrante
                  presupuesto = { presupuesto }
                  sobrante = { sobrante }
                
                />



              </div>
            </div>
          )
          }
        </div>
      </header>
    </div>
  );
}

export default App;
